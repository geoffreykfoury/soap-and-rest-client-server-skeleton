
//notes

// need to fix reqs. for updateRequirementsList  
        // Need to be updated from table to hover over 
            // see lines 154 and 550

$(document).ready(function () {
    //sources();

    var activeEntities = ["Person", "Organization"];

    loadSideBar();

    $('[data-toggle=offcanvas]').click(function () {
        $('.row-offcanvas').toggleClass('active');
    });

//** THIS FUNCTION LOADS THE SIDEBAR DYNAMICALLY AND ALSO ASSIGNS VALIDATION TO THE INPUTS ACCORDINGLY
    function loadSideBar() {

        jsonMeta = {
            "LCM": {
                "Person": {
                    "person.givenName": {
                        "vDescription": "Can only include alpha characters and must be more than 3 and less than 45 characters",
                        "label": "First Name",
                        "pattern": "^[\\w\\s]+$",
                        "min": "3",
                        "max": "45",
                        "required": true
                    },
                    "person.middleName": {
                        "vDescription": "Can only include alpha characters and must be less than 45 characters",
                        "label": "Middle Name",
                        "pattern": "^[\\w\\s]+$",
                        "min": "3",
                        "max": "45",
                        "required": false
                    },
                    "person.surname": {
                        "vDescription": "Can only include alpha characters be more than 3 less than 45 characters",
                        "label": "Last Name",
                        "pattern": "^[\\w\\s]+$",
                        "min": "3",
                        "max": "45",
                        "required": true
                    },
                    "eyeColor": {
                        "vDescription": "Can only include alpha characters and must be less than 45 characters",
                        "label": "Eye Color",
                        "pattern": "[A-Za-z]*",
                        "min": "0",
                        "max": "10",
                        "required": false
                    },
                    "dob": {
                        "vDescription": "Can only include alpha characters and must be less than 45 characters",
                        "label": "Date of Birth",
                        "pattern": "[A-Za-z]*",
                        "min": "0",
                        "max": "10",
                        "required": false
                    },
                    "gender": {
                        "vDescription": "Can only include alpha characters and must be less than 45 characters",
                        "label": "Gender",
                        "pattern": "[A-Za-z]*",
                        "min": "0",
                        "max": "10",
                        "required": false
                    },
                    "nationality": {
                        "vDescription": "Can only include alpha characters and must be less than 45 characters",
                        "label": "Nationality",
                        "pattern": "[A-Za-z]*",
                        "min": "0",
                        "max": "10",
                        "required": false
                    },
                    "weight": {
                        "vDescription": "Must include at least 2 integers and max 3 integers",
                        "label": "Weight",
                        "pattern": "[0-9]{2,3}",
                        "min": "0",
                        "max": "11",
                        "required": false
                    },
                    "height": {
                        "vDescription": "Must include at least 2 integers and max 3 integers",
                        "label": "Height",
                        "pattern": "[0-9]{2,3}",
                        "min": "0",
                        "max": "11",
                        "required": false
                    }
                },
                "Organization": {
                    "organization.orgName": {
                        "vDescription": "Can only include alpha characters and must be less than 45 characters",
                        "label": "Organization Name",
                        "pattern": "^[\\w\\s]+$",
                        "min": "3",
                        "max": "45",
                        "required": true
                    }
                }
            }
        };

        searchable = jsonMeta["LCM"];

        if (jQuery.type(searchable) !== "string") {



            $.each(activeEntities, function (i, x) {
                var created = false;

                $("#operations-table").append("<li id='view-" + x + "' class=''><a id='" + x + "' href='#'>" + x + "<span style='padding-left:10px' class='eye glyphicon glyphicon-ok'></span></a></li>");
                $("#req-table tbody").append("<tr id='" + x + "-reqs'><td>" + x + "</td></tr>");
                for (y in searchable[x]) {
                    var field = $("input[name='" + y + "']");
                    //not optimized - should originally get parent div by Source Id and then assign change child elements?
                    //make visible
                    field.closest("div .input-parent").removeClass('inactive');
                    field.closest("div .input-parent").addClass('active');
                    field.closest("div .input-parent").css('display', '');
                    $("Select[name='" + y + "']").closest("div .input-parent").removeClass('inactive');
                    $("Select[name='" + y + "']").closest("div .input-parent").addClass('active');
                    $("Select[name='" + y + "']").closest("div .input-parent").css('display', '');

                    //add patterns and validation
                    field.attr('pattern', searchable[x][y]["pattern"]);
                    if (searchable[x][y]["required"]) {
                        field.attr('required', '');
                        field.attr("id", x + "-reqs");
                        if (!created) {
                            $("#req-table tbody #" + x + "-reqs").append("<td>" + y + "</td>");
                            created = true;
                        } else {
                            $("#req-table tbody #" + x + "-reqs > td").eq(1).append(" " + y);
                        }
                    }
                    field.attr('maxLength', searchable[x][y]["max"]);
                    field.attr('minLength', searchable[x][y]["min"]);
                    field.attr('title', searchable[x][y]["vDescription"]);

                }
            });
        }
    }

    function updateRequirementsList(entity) {

//        $("#view-" + entity).attr();
//
//        $("#req-table tbody").append("<tr id='" + entity + "-reqs'><td>" + entity + "</td></tr>");
//        var created = false;
        var toolTipReqs = "";
        for (y in searchable[entity]) {
            if (searchable[entity][y]["required"]) {

                toolTipReqs += y + " ";

//                if (!created) {
//                    $("#req-table tbody #" + entity + "-reqs").append("<td>" + y + "</td>");
//                    created = true;
//                } else {
//                    $("#req-table tbody #" + entity + "-reqs > td").eq(1).append(" " + y);
//                }
            }
        }
        $("#view-" + entity).attr('title', toolTipReqs);

    }


//*********************************************************************************************************************************************************
// NEWS API                  NEWS API                    NEWS API            NEWS API                NEWS API                    NEWS API 
//*********************************************************************************************************************************************************

    function sources() {

        var sources = "";
        var sort = "";
        $.ajax({
            type: 'GET',
            url: contextRoot + '/headlines/sources/',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {

            $.each(data, function (index, Source) {

                $('.sources').append("<option value='" + Source.id + "'>" + Source.name + " | " + Source.category + "</option>");
            });
        });
    }

    $('#headlines-submit').on('click', function (e) {
        $('#article-table').empty();
        e.preventDefault();
        var source = $('#source').val();
        var sort = $('#sort').val();
        $.ajax({
            type: 'GET',
            url: contextRoot + '/headlines/' + sort + '/' + source,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {



            $.each(data, function (index, Article) {


                $('#article-table').append("<tr><td>" + Article.author + "</td><td>" + Article.description + "</td><td>" + Article.title + "</td><td><a href='" + Article.url + "'>Link</a></td><td>" + Article.publishedAt + "</td><td><img style='width:50%; height:auto' class='img-responsive img-thumbnail' src='" + Article.urlToImage + "' alt='Article Image'></tr>");
            });
        });
    });

    $('#filter-source').on('click', function (e) {


        $("#advanced-source").toggle();
    });

    $('#filter-submit').on('click', function (e) {

        $('.sources').empty();
        var category = $('#category').val();
        var language = $('#language').val();
        var country = $('#country').val();
        $.ajax({
            type: 'GET',
            url: contextRoot + '/headlines/filter/' + category + "/" + language + "/" + country,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {

            $.each(data, function (index, Source) {

                $('.sources').append("<option value='" + Source.id + "'>" + Source.name + " | " + Source.category + "</option>");
            });
        });
    });

    $('#multi-article-submit').on('click', function (e) {

        $('#article-table').empty();
        e.preventDefault();
        var sort = $('#sort').val();
        var formData = JSON.stringify({
            sources: $('#multi').val()
        });
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $.ajax({
            type: 'POST',
            url: contextRoot + "/headlines/" + sort,
            data: formData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
                xhr.setRequestHeader(header, token);
            }
        }).success(function (data, status) {

            $.each(data, function (index, Article) {

                $('#article-table').append("<tr><td>" + Article.author + "</td><td>" + Article.description + "</td><td>" + Article.title + "</td><td><a href='" + Article.url + "'>Link</a></td><td>" + Article.publishedAt + "</td><td><img style='width:50%; height:auto' class='img-responsive img-thumbnail' src='" + Article.urlToImage + "' alt='Article Image'></tr>");
            });
        });
    });

//*********************************************************************************************************************************************************
// END NEWS API          END NEWS API           END  NEWS API          END  NEWS API          END NEWS API           END NEWS API 
//*********************************************************************************************************************************************************
    $('#org-submit').on('click', function (e) {

        e.preventDefault();
        $('#organization-table').empty();
        $('#article-table').empty();
        $('#headlines').hide();
        $('#org-error').hide();
        $('#org-error').empty();
        var news = false;
        if ($('#news-search').is(':checked')) {

            news = true;
        }


        var orgName = $('#org-value').val();
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $.ajax({
            type: 'POST',
            url: contextRoot + "/organization/" + orgName + "/" + news,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
                xhr.setRequestHeader(header, token);
            }
        }).success(function (data, status) {

            $.each(data, function (index, org) {

                if (org.country) {
                    $('#organization-table').append("<tr><td>" + org.name + "</td><td>" + org.sourceId + "</td><td>" + org.country + "</td><td>" + org.dateIncorporated + "</td><td>" + org.stateOfCharter + "</td><td><button type='button' class='info btn btn-primary btn-sm' value='" + org.dataItemId + "'>Info</button></tr>");
                } else {
                    $('#org-error').show();
                    $('#org-error').append("<strong>Whoops!</strong> " + org.advisoryTxt);
                }

                if (org.title) {
                    $('#headlines').show();
                    $('#article-table').append("<tr><td>" + org.author + "</td><td>" + org.description + "</td><td>" + org.title + "</td><td><a href='" + org.url + "'>Link</a></td><td>" + org.publishedAt + "</td><td><img style='width:50%; height:auto' class='img-responsive img-thumbnail' src='" + org.urlToImage + "' alt='org Image'></tr>");
                }


            });
        });
    });
//****************************************************************************************************************************************************************
//            RETRIEVE                                                  RETRIEVE                                            RETRIEVE
//**************************************************************************************************************************************************************** 
//ORG MODAL RETRIEVE
    $('table').on('click', '.org-info', function (e) {

        var recordId = $(this).attr('value');

        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");


        $.ajax({
            type: 'POST',
            url: contextRoot + "/org/" + recordId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
                xhr.setRequestHeader(header, token);
            }
        }).success(function (data, status) {

            $.each(data, function (index, org) {

                if (org.dataItemId) {
                    var infoModal = "<div id='modal" + org.dataItemId + "' class='modal fade'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'>\
                        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'><i class='fa fa-remove fa-fw'></i></span><span class='sr-only'>close</span></button>\
                        <h4 id='modalTitle' class='modal-title'>" + org.sourceId + " " + org.dataItemId + " | ALABAMA TEXTILES LTD</h4>\
                        </div><div id='modalBody' class='modal-body'><div><b>Address:</b> " + org.address + "</div><div><b>Address Type:</b> " + org.addressType + "</div><div><b>Alias:</b> " + org.alias + "</div><div><b>Status:</b> " + org.status + "</div>\
                        <div><b>Org. Officer :</b> " + org.officerName + "</div><div><b>Org. Officer Title :</b> " + org.officerTitle + "</div></div>\
                        <div class='modal-footer'></div></div></div></div>";
                    //<div><b>Employee Id:</b> " + org.employeeId + "</div>
                    $('body').append(infoModal);
                    $('#modal' + org.dataItemId).modal();

                } else {
                    $('#org-error').show();
                    $('#org-error').append("<strong>Whoops!</strong> " + org.advisoryTxt);
                }
            });
        });
    });
    //END ORG MODAL RETRIEVE

    //ORG FULL RETRIEVE
    $('.org-full').on('click', function (e) {
        var recordId = $(this).attr('value');

        window.location.replace(contextRoot + "/org/retrieve/" + recordId);

    });

    //PRSN MODAL QUICK RETRIEVE 
    $('table').on('click', '.prsn-info', function (e) {

        var recordId = $(this).attr('value');

        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");


        $.ajax({
            type: 'POST',
            url: contextRoot + "/person/" + recordId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
                xhr.setRequestHeader(header, token);
            }
        }).success(function (data, status) {

            $.each(data, function (index, Person) {

                if (Person.dataId) {
                    var infoModal = "<div id='modal" + Person.addressType + "' class='modal fade'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'>\
                        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'><i class='fa fa-remove fa-fw'></i></span><span class='sr-only'>close</span></button>\
                        <h4 id='modalTitle' class='modal-title'>" + Person.dataId + " | " + Person.fullName + "</h4>\
                        </div><div id='modalBody' class='modal-body'><div><b>Address:</b> " + Person.address + "</div><div><b>Address Type:</b> " + Person.addressType + "</div>\
                        <div><b>Eye Color:</b> " + Person.eyeColor + "</div>\
                        <div><b>Hair Color:</b> " + Person.hairColor + "</div>\
                        <div><b>Weight Color:</b> " + Person.weight + "</div>\
                        </div>\
                        <div class='modal-footer'></div></div></div></div>";
                    $('body').append(infoModal);
                    $('#modal' + Person.addressType).modal();

                } else {
                    $('#error').show();
                    $('#error').append("<strong>Whoops!</strong> " + Person.advisoryTxt);
                }
            });
        });
    });
    //END PRSN QUICK RETRIEVE

    //PERSON FULL RETRIEVE
    $('.prsn-full').on('click', function (e) {
        var recordId = $(this).attr('value');

        window.location.replace(contextRoot + "/person/retrieve/" + recordId);

    });
    //END PERSON FULL RETRIEVE

//**************************************************************************************************************************************************************** 
    //           END RETRIEVE                                                END RETRIEVE                                          END RETRIEVE   
//*************************************************************************************************************************************************************************

//*************************************************************************************************************************************************************************
//  SELECTION TOGGLING AND UI COMPONENT DISPLAYS        SELECTION TOGGLING AND UI COMPONENT DISPLAYS                SELECTION TOGGLING AND UI COMPONENT DISPLAYS
//*************************************************************************************************************************************************************************


    $('.src-slct').on("change", function (e) {
        var slcts = $('.src-slct').val();
        var pills = $('.nav-pills li a');

        if (slcts !== null) {

            $.each(pills, function (e) {
                var id = $(this).attr('id');
                $('.' + id).removeClass('active').addClass('inactive');
                $(this).find('span').removeClass('eye glyphicon glyphicon-ok').addClass('close glyphicon glyphicon-remove');

            });

            $.each(slcts, function (i, val) {
                $('.' + val).removeClass('inactive').addClass('active');
                $('#' + val).removeClass('inactive').addClass('active');
                $('#' + val).find('span').removeClass('close glyphicon glyphicon-remove');
                $('#' + val).find('span').addClass('glyphicon glyphicon-ok');
            });

        } else {
            $.each(pills, function (e) {
                var id = $(this).attr('id');
                $('.' + id).removeClass('inactive').addClass('active');
                $(this).find('span').removeClass('close glyphicon glyphicon-remove').addClass('eye glyphicon glyphicon-ok');
            });

        }
        inActvDsply();

    });

    $('.nav-pills li a').click(function () {

        var slcts = $('.src-slct').val();

        var id = $(this).attr('id');

        if ($('.' + id).hasClass('active')) {
            $('.' + id).removeClass('active').addClass('inactive');
            $(this).find('span').removeClass('eye glyphicon glyphicon-ok').addClass('close glyphicon glyphicon-remove');
            toggSlctsRmv();
            $(".inactive :input").val("");
            $(".inactive :input").attr('type', 'hidden');
            $("#" + id + "-reqs").remove();
            $("#prsn-form").validator('update');
//          $("#prsn-form").validator('validate');

        } else {
            $('.' + id).removeClass('inactive').addClass('active');
            $(this).find('span').removeClass('close glyphicon glyphicon-remove').addClass('eye glyphicon glyphicon-ok');
            $('.' + id + " :input").attr('type', 'text');
            $("#prsn-form").validator('update');
            $("#prsn-form").validator('validate');
            toggSlctsAdd();
            updateRequirementsList(id);

        }

        function toggSlctsRmv() {
            if (slcts !== null) {
                $.each(slcts, function (i, val) {
                    if (val === id) {
                        $('#sidebar > div:nth-child(2) > span > span.selection > span > ul > li.select2-selection__choice[title=' + id + '] > span').click();
                        $('#src-slct').select2("close");
                    }
                });
            }
        }
        function toggSlctsAdd() {
            if (slcts !== null) {

                slcts.push(id);
                $('#src-slct').val(slcts).trigger("change");

            }
        }
        var entities = "";
        $('#operations-table li a span.glyphicon-ok').each(function () {
            var source = $(this).parent().text();
            entities += "|" + source;

        });

        $("#entities").val(entities);

        inActvDsply();

    });


    function inActvDsply() {
        $('.inactive').hide(300, "swing");
        $('.active').show(300, "swing");
    }

//*************************************************************************************************************************************************************************
// END SELECTION TOGGLING AND UI COMPONENT DISPLAYS        END SELECTION TOGGLING AND UI COMPONENT DISPLAYS              END SELECTION TOGGLING AND UI COMPONENT DISPLAYS
//*************************************************************************************************************************************************************************

    //Requirements

    $('input').focusout(function () {
        var thisInput = $(this);
        var id = thisInput.attr('id');
        var name = thisInput.attr('name');
        if (thisInput.attr('required') && !thisInput.closest('div .form-group').hasClass('has-error')) {

            var txt = $("#req-table tbody #" + id + "> td").eq(1).text().replace(name, '');
            $("#req-table tbody #" + id + "> td").eq(1).text(txt);
            if (txt.trim() === "") {
                $("#req-table tbody #" + id + "> td").eq(1).append('<td style="padding-left:8em"><i style="color:green; text-shadow: 2px 2px 5px grey" class="glyphicon glyphicon-ok"></i></td>');
            }
        }
        if (thisInput.attr('required') && thisInput.closest('div .form-group').hasClass('has-error')) {
            var txt = $("#req-table tbody #" + id + "> td").eq(1).text();

            if (!~txt.indexOf(name)) {

                $("#req-table tbody #" + id + "> td").eq(1).text(txt += " " + name);
            }
        }
    });

    //get all that are selected
//        $('.nav-pills li a span.glyphicon-ok').each(function () {
//            var source = $(this).parent().text();
////
//            var required = jsonMeta["LCM"][source]["firstname"];
////
////            console.log(required);
//
//            if (jQuery.type(required) !== "string") {
//
//                var reqPrnt = "";
//                for (x in required) {
//                    //now check array if valid input has added a class of validated - validated elements with valid class added to array        
//
//                    //loop through valid array and see if x exists.
//                    //if not, list add to reqPrnt
//
//                    reqPrnt += "[" + x + "] ";
////                    
//                }
//            };
//                });
//                $('#req-table tbody').append("<tr><td>" + $( this ).parent().text() + "</td><td>" + reqPrnt + "</td></tr>");

//    });
//    }

//Form Submit

//    $('#form-submit').on('click', function () {
//
////        var person = $('#prsn-form').serialize();
//
//        var person = {};
//
//        var data = $('#prsn-form').serialize().split("&");
//
// 
//        for (var key in data)
//        {
//            person[data[key].split("=")[0]] = data[key].split("=")[1];
//        }
//
//             person = JSON.stringify(person);
//
//
//        var token = $("meta[name='_csrf']").attr("content");
//        var header = $("meta[name='_csrf_header']").attr("content");
//
//
//        $.ajax({
//            type: 'POST',
//            url: contextRoot + "/person/query",
//            data: person,
//            dataType: 'json',
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader("Accept", "application/json");
//                xhr.setRequestHeader("Content-type", "application/json");
//                xhr.setRequestHeader(header, token);
//            }
//        }).success(function (data, status) {
//            
//            window.query = data;
//
//            window.location.href = contextRoot + "/person-results";
//        });
//    });


//    $('#srchRslCol').on('click', '.index', function (e) {
//        e.preventDefault();
//
////        window.location.href = "http://localhost:9090/person-results";
//
//
////        var formData = globalVar;
//        formData = JSON.stringify(formData);
////
//        var token = $("meta[name='_csrf']").attr("content");
//        var header = $("meta[name='_csrf_header']").attr("content");
//        $.ajax({
//            type: 'POST',
//            url: contextRoot + "/person/results",
//            data: formData,
////            dataType: 'html',
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader("Accept", "application/json");
//                xhr.setRequestHeader("Content-type", "application/json");
//                xhr.setRequestHeader(header, token);
//            }
//
//        });
//
//
//
//    });





});