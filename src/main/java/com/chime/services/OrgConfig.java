
package com.chime.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class OrgConfig {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("chime.wsdl");
		return marshaller;
	}

	@Bean
	public OrgsClient orgsClient(Jaxb2Marshaller marshaller) {
		OrgsClient client = new OrgsClient();
		client.setDefaultUri("http://REDACTED");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}

