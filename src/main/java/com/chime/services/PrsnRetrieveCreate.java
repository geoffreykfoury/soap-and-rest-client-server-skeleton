package com.chime.services;

import chime.wsdl.DataItemIdentifierType;
import chime.wsdl.IdentifierType;
import chime.wsdl.OutputEntityType;
import chime.wsdl.RequestMetadataType;
import chime.wsdl.RetrieveRequestType;
import chime.wsdl.SourceIdType;
import chime.wsdl.UserMetadataType;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class PrsnRetrieveCreate {

    private static final org.slf4j.Logger CHIMELOG = LoggerFactory.getLogger(OrgsClient.class);

    public RetrieveRequestType createPrsnRetrieve(String dataId) {
        RetrieveRequestType prsnRetrieve = new RetrieveRequestType();

        CHIMELOG.debug("Creating retrieve for person query | " + dataId);

        try {

            RequestMetadataType meta = new RequestMetadataType();
            meta.setMessageId("org-retrieve");
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            meta.setTimeStamp(xmlDate);
            meta.setIncludeSourceMessageInResult(Boolean.TRUE);

            UserMetadataType mt = new UserMetadataType();
            mt.setUserId("566452");
            mt.setUserPhone("703-927-1111");
            mt.setPurpose("P");
            mt.setRequesterName("Alfred");
            mt.setRequestingOrganization("Doe");

            DataItemIdentifierType dataType = new DataItemIdentifierType();
//          dataType.setDataItemFilter(value);
            dataType.setEntityId(OutputEntityType.PERSON);
            dataType.setSourceId(SourceIdType.LCM);
            dataType.setIdType(IdentifierType.RECORD_ID);
            dataType.setDataItemId(dataId);

            prsnRetrieve.setUserMetadata(mt);
            prsnRetrieve.setRequestMetadata(meta);
            prsnRetrieve.setDataItemIdentifier(dataType);

        } catch (DatatypeConfigurationException ex) {
            CHIMELOG.error("Error creating retrieve for - DATE PARSE | " + dataId + " " + ex.toString());

        } catch (Exception ex) {
            CHIMELOG.error("Error creating retrieve for | " + dataId + " " + ex.toString());
        }

        return prsnRetrieve;

    }

}
