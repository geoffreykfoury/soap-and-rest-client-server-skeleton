package com.chime.services;

import chime.wsdl.ObjectFactory;

import chime.wsdl.OrganizationSearchParametersType;
import chime.wsdl.OrganizationSearchRequestType;
import chime.wsdl.OrganizationSearchResultType;
import chime.wsdl.SearchConstraintsType;
import chime.wsdl.SearchRequestMetadataType;
import chime.wsdl.SourceIdType;
import chime.wsdl.UserMetadataType;
import com.chime.models.ChimeAdvisoryError;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class OrgsClient extends WebServiceGatewaySupport {

    private ChimeAdvisoryError error = null;
    private final ObjectFactory CHIMEOBJECTFACTORY = new ObjectFactory();

    private static final Logger CHIMELOG = LoggerFactory.getLogger(OrgsClient.class);

    @Autowired
    ServiceInterceptor oi;
    
    
    public ChimeAdvisoryError getError() {
        return error;
    }

    public OrganizationSearchResultType getOrganization(String organizationName) {

        OrganizationSearchResultType responseValue = null;

        try {
            SearchRequestMetadataType sr = new SearchRequestMetadataType();

            sr.setMessageId("sssyoooos");
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            sr.setTimeStamp(xmlDate);
            sr.setIncludeSourceMessageInResult(Boolean.TRUE);
            sr.setIncludeDataItems(Boolean.TRUE);

            UserMetadataType mt = new UserMetadataType();
            mt.setUserId("566452");
            mt.setUserPhone("703-927-9841");
            mt.setPurpose("P");
            mt.setRequesterName("Alfred");
            mt.setRequestingOrganization("Doe");

            OrganizationSearchParametersType os = new OrganizationSearchParametersType();
            os.setOrganizationName(organizationName);
            
            // This will need to be global as metadata for all reuests
            SearchConstraintsType constraints = new SearchConstraintsType();
            constraints.getSource().add(SourceIdType.LCM);            
            constraints.getEntity().add("Person");
           
           
            OrganizationSearchRequestType request = new OrganizationSearchRequestType();
            request.setSearchParameters(os);
            request.setUserMetadata(mt);
            request.setSearchRequestMetadata(sr);
            request.setSearchConstraints(constraints);
            

            CHIMELOG.info("Requesting result for organization search | " + os.getOrganizationName());

            getWebServiceTemplate().setCheckConnectionForError(false);
            getWebServiceTemplate().setCheckConnectionForFault(false);

            ClientInterceptor[] interceptors = new ClientInterceptor[1];
            interceptors[0] = oi;
            getWebServiceTemplate().setInterceptors(interceptors);

            JAXBElement initalResponse = (JAXBElement) getWebServiceTemplate()
                    .marshalSendAndReceive("http://REDACTED", CHIMEOBJECTFACTORY.createOrganizationSearchRequest(request));

            responseValue = (OrganizationSearchResultType) initalResponse.getValue();

        } catch (DatatypeConfigurationException ex) {
            CHIMELOG.error("Error running Organization service | " + ex.getMessage());
        } catch (UnsupportedOperationException ex) {
            this.error = oi.error;
            CHIMELOG.error("Error response from Chime: " + this.error.getAdvisoryTxt());
            throw new UnsupportedOperationException("");
        }

        return responseValue;

    }

}
