
//package com.chime.services;
//
//
//import chime.wsdl.GetInfoByZIP;
//import com.chime.models.Response;
//import com.chime.models.Zips;
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
//import javax.xml.transform.Result;
//import org.springframework.context.annotation.Bean;
//
//import org.springframework.oxm.jaxb.Jaxb2Marshaller;
//import org.springframework.stereotype.Service;
//import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
//import org.springframework.ws.soap.client.core.SoapActionCallback;
//
///**
// *
// * @author Geoffrey Ford Kfoury
// */

//THIS IS AN EXAMPLE OF MANUALLY MARHSALLING A RESPONSE *********************************************************

//@Service
//public class Zip extends WebServiceGatewaySupport {
//
//    @Bean
//    public Jaxb2Marshaller marshaller() {
//        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//        // this package must match the package in the <generatePackage> specified in
//        // pom.xml
//        marshaller.setContextPath("com.chime.models");
//        return marshaller;
//    }
//
//    public Response zipResponse(Zips zipCode) throws JAXBException, Exception {
//
//        this.setDefaultUri("http://www.webservicex.net/uszip.asmx");
//
//        this.setMarshaller(this.marshaller());
//
////        JAXBContext jaxbContext = JAXBContext.newInstance(ZipCodeResponse.class);
//
////        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//
//        this.setUnmarshaller(this.marshaller());
//
////
////        SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory(
////                MessageFactory.newInstance());
////        
//// 
////        messageFactory.afterPropertiesSet();
////
////        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
////        webServiceTemplate.setDefaultUri("http://www.webservicex.net/uszip.asmx");
////
////    JAXBContext context =  JAXBContext.newInstance(new Class[]{com.chime.models.ZipCode.class, com.chime.models.ZipCodeResponse.class});
////        JAXBContext context = JAXBContext.newInstance("src/main/java/com/chime/models");
////        marshaller.setPackagesToScan(new String[]{"com.chime.models"});
//////                marshaller.afterPropertiesSet();
////        marshaller.setContextPath("com.chime.models");
////        marshaller.setClassesToBeBound(new Class[] {com.chime.models.ZipCode.class, com.chime.models.ZipCodeResponse.class});
////        marshaller.afterPropertiesSet();
////
////        webServiceTemplate.setMarshaller(marshaller);
////        webServiceTemplate.setUnmarshaller(marshaller);
//        Response test = (Response) getWebServiceTemplate().marshalSendAndReceive("http://www.webservicex.net/uszip.asmx", zipCode, new SoapActionCallback("http://www.webserviceX.NET/GetInfoByZIP"));
//
//
//        return test;
//
//    }
//}

// THIS IS AN EXAMPLE OF UNMARSHALLING A ElementNSImpl **********************************************************************************

//public class ElementNSImplSample extends WebServiceGatewaySupport {
//
//    private static final Logger log = LoggerFactory.getLogger(ElementNSImplSample.class);
//
//    public NewDataSet getZip(String zipcode) throws JAXBException {
//
//        GetInfoByZIP request = new GetInfoByZIP();
//        request.setUSZip(zipcode);
//
//        log.info("Requesting quote for " + zipcode);
//
//        GetInfoByZIPResponse test = (GetInfoByZIPResponse) getWebServiceTemplate()
//                .marshalSendAndReceive("http://www.webservicex.net/uszip.asmx", request, new SoapActionCallback("http://www.webserviceX.NET/GetInfoByZIP"));
//
//        ElementNSImpl eleNsImplObject = (ElementNSImpl) test.getGetInfoByZIPResult().getContent().get(0);
//        org.w3c.dom.Document document = eleNsImplObject.getOwnerDocument().getFirstChild().getOwnerDocument();
//
//        JAXBContext jaxbContext = JAXBContext.newInstance(NewDataSet.class);
//
//        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//
//        NewDataSet tables = (NewDataSet) jaxbUnmarshaller.unmarshal(document);
//
//        return tables;
//    }
//
//}

// RAW JAVA SOAP REQUEST AND RESPONSE  -   ******************************************************************************

////             String soapXml = new String(encoded, StandardCharsets.UTF_8);
//      byte[] encoded = test.getBytes();
//    SOAPConnectionFactory soapConnectionFactory =
//            SOAPConnectionFactory.newInstance();
//    java.net.URL endpoint = new URL("http://34.195.126.254:8080/query");
//    SOAPConnection connection = soapConnectionFactory.createConnection();
//    MessageFactory factory = MessageFactory.newInstance();
//    SOAPMessage message = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(encoded));
//    AttachmentPart attachment = message.createAttachmentPart();
//    attachment.setContent("sm_content", "text/plain");
//    attachment.setContentId("1.9f910338bf0cac0e783bfdec7e53be9237684caa8f8f4e6d@apache.org");
//    message.addAttachmentPart(attachment);
//    SOAPMessage response = connection.call(message, endpoint);
//    ByteArrayOutputStream out = new ByteArrayOutputStream();
//    response.writeTo(out);
//    String strMsg = new String(out.toByteArray());
//    
//    String testing = strMsg;
//    Node node = response.getSOAPBody();
//    
//    org.w3c.dom.Node nodes = node.getFirstChild();
//    
//    
//   
//        
//        JAXBContext jc = JAXBContext.newInstance("chime.wsdl");
//        
       
        
//        Unmarshaller unmarshaller = jc.createUnmarshaller();
//
//        Object testings3 = unmarshaller.unmarshal(node.getFirstChild());
//        
//       JAXBElement<SearchResultType> hello = unmarshaller.unmarshal(node.getFirstChild(), SearchResultType.class);      
//       hello.getClass();
//       hello.getDeclaredType().cast(testings3)

//       JAXBElement<Object> elemental =  (JAXBElement<Object>) unmarshaller.unmarshal(node.getFirstChild());
       
//       Object hello = elemental;
       
       

//            JAXBElement<SearchRequestType> payload = CHIMEOBJECTFACTORY.createSearchRequest(request);
//            Result result = result.
//getWebServiceTemplate().sendSourceAndReceiveToResult("http://34.195.126.254:8080/query", source, result);
//
//StringWriter sw = (StringWriter) result.getWriter(); 
//StringBuffer sb = sw.getBuffer(); 
//String finalstring = sb.toString();



//            System.out.println(finalstring);