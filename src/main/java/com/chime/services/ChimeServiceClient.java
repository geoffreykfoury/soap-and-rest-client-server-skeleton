package com.chime.services;

import chime.wsdl.ObjectFactory;
import chime.wsdl.PersonSearchRequestType;
import chime.wsdl.PersonSearchResultType;
import chime.wsdl.RetrieveRequestType;
import chime.wsdl.RetrieveResultType;

import chime.wsdl.SearchRequestType;
import chime.wsdl.SearchResultType;
import com.chime.models.ChimeAdvisoryError;
import javax.xml.bind.JAXBElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class ChimeServiceClient extends WebServiceGatewaySupport {

    private ChimeAdvisoryError error = null;
    private final ObjectFactory CHIMEOBJECTFACTORY = new ObjectFactory();

    private static final Logger CHIMELOG = LoggerFactory.getLogger(OrgsClient.class);

    @Autowired
    ServiceInterceptor interceptor;


    public SearchResultType searchChime(SearchRequestType request){

        SearchResultType responseValue = null;

        try {
            CHIMELOG.info("Requesting result for request query | " + request.getSearchParameters().getGivenName() + " " + request.getSearchParameters().getSurname());

            getWebServiceTemplate().setCheckConnectionForError(false);
            getWebServiceTemplate().setCheckConnectionForFault(false);

            ClientInterceptor[] interceptors = new ClientInterceptor[1];
            interceptors[0] = interceptor;
            getWebServiceTemplate().setInterceptors(interceptors);

            JAXBElement<SearchResultType> initalResponse = (JAXBElement) getWebServiceTemplate()
                    .marshalSendAndReceive(CHIMEOBJECTFACTORY.createSearchRequest(request));

            responseValue = (SearchResultType) initalResponse.getValue();

        } catch (UnsupportedOperationException ex) {
            this.error = interceptor.error;
            CHIMELOG.error("Error response from Chime: " + this.getError().getAdvisoryTxt());
            throw new UnsupportedOperationException("");
        }

        return responseValue;

    }
    
     public SearchResultType searchChimePrsn(PersonSearchRequestType request){

        SearchResultType responseValue = null;

        try {
            CHIMELOG.info("Requesting result for request query | " + request.getSearchParameters().getGivenName() + " " + request.getSearchParameters().getSurname());

            getWebServiceTemplate().setCheckConnectionForError(false);
            getWebServiceTemplate().setCheckConnectionForFault(false);

            ClientInterceptor[] interceptors = new ClientInterceptor[1];
            interceptors[0] = interceptor;
            getWebServiceTemplate().setInterceptors(interceptors);

            JAXBElement<SearchResultType> initalResponse = (JAXBElement) getWebServiceTemplate()
                    .marshalSendAndReceive(CHIMEOBJECTFACTORY.createPersonSearchRequest(request));

            responseValue = initalResponse.getValue();

        } catch (UnsupportedOperationException ex) {
            this.error = interceptor.error;
            CHIMELOG.error("Error response from Chime: " + this.getError().getAdvisoryTxt());
            throw new UnsupportedOperationException("");
        }

        return responseValue;

    }

    //RETRIEVE
    public RetrieveResultType retrieveDetails(RetrieveRequestType retrieveId) {

       RetrieveResultType retrieveValue = null;

        try {
            CHIMELOG.info("Requesting retrieve result for person  | " + retrieveId.getDataItemIdentifier().getDataItemId());

            getWebServiceTemplate().setCheckConnectionForError(false);
            getWebServiceTemplate().setCheckConnectionForFault(false);

            ClientInterceptor[] interceptors = new ClientInterceptor[1];
            interceptors[0] = interceptor;
            getWebServiceTemplate().setInterceptors(interceptors);

            JAXBElement initalResponse = (JAXBElement) getWebServiceTemplate()
                    .marshalSendAndReceive("http://REDACTED", CHIMEOBJECTFACTORY.createRetrieveRequest(retrieveId));

            retrieveValue = (RetrieveResultType) initalResponse.getValue();

        } catch (UnsupportedOperationException ex) {
            this.error = interceptor.error;
            CHIMELOG.error("Error response from Chime: " + this.getError().getAdvisoryTxt());
            throw new UnsupportedOperationException("");
        }

        return retrieveValue;

    }

    public ChimeAdvisoryError getError() {
        return error;
    }

}
