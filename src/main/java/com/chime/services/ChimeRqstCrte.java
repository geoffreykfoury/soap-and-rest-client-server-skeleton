package com.chime.services;

import chime.wsdl.SearchConstraintsType;
import chime.wsdl.SearchParametersType;
import chime.wsdl.SearchRequestMetadataType;
import chime.wsdl.SearchRequestType;
import chime.wsdl.SourceIdType;
import chime.wsdl.UserMetadataType;
import com.chime.models.ChimeFormQuery;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.springframework.stereotype.Service;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class ChimeRqstCrte {

    public SearchRequestType crteChimeRqst(ChimeFormQuery fq) {
        SearchRequestType request = new SearchRequestType();
        try {

            SearchParametersType params = new SearchParametersType();

            if (fq.getPerson() != null) {

                params.setGivenName(fq.getPerson().getGivenName());
//              params.setMiddleName(person.getMiddleName());
                params.setSurname(fq.getPerson().getSurname());
//              params.setEyeColor(EyeColorCodeType.BL);
//              params.setNationality(CountryAlpha2CodeSimpleType.AL);
//              params.setHeight(BigInteger.ONE);

            }

            if (fq.getOrganization() != null) {
                params.setOrganizationName(fq.getOrganization().getOrgName());

            }

            SearchRequestMetadataType sr = new SearchRequestMetadataType();

            sr.setMessageId("CHIME-DEMO");
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            sr.setTimeStamp(xmlDate);
            sr.setIncludeSourceMessageInResult(Boolean.TRUE);
            sr.setIncludeDataItems(Boolean.TRUE);

            SearchConstraintsType constraints = new SearchConstraintsType();
            if (fq.getEntities() != null) {

                for (String entity : fq.getEntities().split(Pattern.quote("|"))) {

                    if (!entity.equals("")) {
                        constraints.getEntity().add(entity.trim());

                    }

                }

            }

            constraints.getSource().add(SourceIdType.LCM);
//            constraints.getEntity().add("Organization");
//            constraints.getEntity().add("Person");

            UserMetadataType mt = new UserMetadataType();
            mt.setUserId("123456");
            mt.setUserPhone("703-927-9841");
            mt.setPurpose("P");
            mt.setRequesterName("John");
            mt.setRequestingOrganization("Deere");

            request.setSearchParameters(params);
            request.setSearchRequestMetadata(sr);
            request.setUserMetadata(mt);
            request.setSearchConstraints(constraints);

        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(ChimeRqstCrte.class.getName()).log(Level.SEVERE, null, ex);
        }

        return request;

    }
    
        // DIRECT PERSON SEARCH
    
//    public PersonSearchRequestType crteChimePrsnRqst(ChimeFormQuery fq) {
//        PersonSearchRequestType request = new PersonSearchRequestType();
//        try {
//
//            PersonSearchParametersType params = new PersonSearchParametersType();
//
//            if (fq.getPerson() != null) {
//
//                params.setGivenName(fq.getPerson().getGivenName());
////              params.setMiddleName(person.getMiddleName());
//                params.setSurname(fq.getPerson().getSurname());
////              params.setEyeColor(EyeColorCodeType.BL);
////              params.setNationality(CountryAlpha2CodeSimpleType.AL);
////              params.setHeight(BigInteger.ONE);
//
//            }
//
//            SearchRequestMetadataType sr = new SearchRequestMetadataType();
//
//            sr.setMessageId("CHIME-DEMO");
//            GregorianCalendar c = new GregorianCalendar();
//            c.setTime(new Date());
//            XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
//
//            sr.setTimeStamp(xmlDate);
//            sr.setIncludeSourceMessageInResult(Boolean.TRUE);
//            sr.setIncludeDataItems(Boolean.TRUE);
//
//            SearchConstraintsType constraints = new SearchConstraintsType();
// 
////            constraints.getEntity().add("PERSON");
//   
//            constraints.getSource().add(SourceIdType.LCM);
////            constraints.getEntity().add("Organization");
////            constraints.getEntity().add("Person");
//
//            UserMetadataType mt = new UserMetadataType();
//            mt.setUserId("123456");
//            mt.setUserPhone("703-927-9841");
//            mt.setPurpose("P");
//            mt.setRequesterName("John");
//            mt.setRequestingOrganization("Deere");
//
//            request.setSearchParameters(params);
//            request.setSearchRequestMetadata(sr);
//            request.setUserMetadata(mt);
//            request.setSearchConstraints(constraints);
//
//        } catch (DatatypeConfigurationException ex) {
//            Logger.getLogger(ChimeRqstCrte.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return request;
//
//    }


}
