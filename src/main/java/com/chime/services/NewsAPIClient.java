package com.chime.services;

import com.chime.models.NewsAPIArticles;
import com.chime.models.NewsAPIMultiSearch;
import com.chime.models.NewsAPIResults;
import java.util.ArrayList;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class NewsAPIClient {

    @Autowired
    RestTemplate restTemplate = new RestTemplate();

    private static Pattern p;

    public NewsAPIResults getHeadlinesMultiSource(NewsAPIMultiSearch sources, String sort, String orgName) {

        NewsAPIResults articles = null;
        NewsAPIResults temp = new NewsAPIResults();
        
        orgName = ".*\\b" + orgName + "\\b.*";
        
        
        p = Pattern.compile(orgName);

        for (int i = 0; i < sources.getSources().size(); i++) {

            
                //make iniital call for news result of specific source
                temp = restTemplate.getForObject("https://newsapi.org/v1/articles?source=" + sources.getSources().get(i) + "&sortBy=" + sort + "&apiKey=f70ba31aca1a4bab88d54a766c9ba433", NewsAPIResults.class);

                for (int j = 0; j < temp.getArticles().size(); j++) {
                //concat headline description and title for faster search
                String headlineText = temp.getArticles().get(j).getDescription().concat(temp.getArticles().get(i).getTitle());

                //check to see if org. name is mentioned in source result - fully initialize class if true
                
                if (p.matcher(headlineText).find()) {
                    if (articles == null) {
                        articles = new NewsAPIResults();
                        articles.setArticles(new ArrayList<>());
                        articles.getArticles().add(temp.getArticles().get(j));
                        continue;
                    }
                    articles.getArticles().add(temp.getArticles().get(j));
                }
            }
        }
        return articles;
    }

}
