package com.chime.services;

import chime.wsdl.DataItemIdentifierType;
import chime.wsdl.IdentifierType;
import chime.wsdl.OutputEntityType;
import chime.wsdl.RequestMetadataType;
import chime.wsdl.RetrieveRequestType;
import chime.wsdl.SourceIdType;
import chime.wsdl.UserMetadataType;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.springframework.stereotype.Service;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class OrgRetrieveCreate {

    public RetrieveRequestType createOrgRetrieve(String dataId) {
       RetrieveRequestType retrieve = new RetrieveRequestType();

        try {
            RequestMetadataType meta = new RequestMetadataType();
            meta.setMessageId("org-retrieve");
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            meta.setTimeStamp(xmlDate);
            meta.setIncludeSourceMessageInResult(Boolean.TRUE);

            UserMetadataType mt = new UserMetadataType();
            mt.setUserId("566452");
            mt.setUserPhone("703-927-1111");
            mt.setPurpose("P");
            mt.setRequesterName("Alfred");
            mt.setRequestingOrganization("Doe");

            DataItemIdentifierType dataType = new DataItemIdentifierType();
//            dataType.setDataItemFilter(value);
            dataType.setSourceId(SourceIdType.LCM);
            dataType.setEntityId(OutputEntityType.ORGANIZATION);
            dataType.setIdType(IdentifierType.RECORD_ID);
            dataType.setDataItemId(dataId);

            retrieve.setUserMetadata(mt);
            retrieve.setRequestMetadata(meta);
            retrieve.setDataItemIdentifier(dataType);

        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(OrgRetrieveCreate.class.getName()).log(Level.SEVERE, null, ex);
        }

        return retrieve;
    }

}
