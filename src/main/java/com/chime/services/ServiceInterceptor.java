package com.chime.services;

import com.chime.models.ChimeAdvisoryError;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Service
public class ServiceInterceptor implements ClientInterceptor {

    protected ChimeAdvisoryError error = null;
    private DOMSource dom = null;
    private Node mainNode = null;
    private XPath xPath = XPathFactory.newInstance().newXPath();
    private XPathExpression testStr;
    private NodeList advisory = null;
    private Boolean firstRun = true;

    public ServiceInterceptor() {
        this.xPath.setNamespaceContext(new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                if (prefix == null) {
                    throw new NullPointerException("Null prefix");
                } else if ("soap".equals(prefix)) {
                    return "http://schemas.xmlsoap.org/soap/envelope/";
                } else if ("msg".equals(prefix)) {
                    return "http://REDACTED";
                } else if ("chime".equals(prefix)) {
                    return "http://REDACTED";
                } else {
                    return null;
                }
            }

            public String getPrefix(String uri) {
                throw new UnsupportedOperationException();
            }

            public Iterator<?> getPrefixes(String uri) {
                throw new UnsupportedOperationException();
            }
        });
    }

    @Override
    public boolean handleRequest(MessageContext mc) throws WebServiceClientException {
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext mc) throws WebServiceClientException {

            if (mc.hasResponse()) {
                try {

                    this.dom = (DOMSource) mc.getResponse().getPayloadSource();

                    this.mainNode = dom.getNode();

                    this.testStr = this.xPath.compile("/soap:Envelope/soap:Body/*/*/*[local-name()='Advisory']/*");

                    this.advisory = (NodeList) this.testStr.evaluate(this.mainNode, XPathConstants.NODESET);

                    if (this.advisory.getLength() > 0) {
                        
                        this.error = new ChimeAdvisoryError();
                        
                        this.error.setAdvisoryCat(this.advisory.item(0).getTextContent());
                        this.error.setAdvisoryCde(this.advisory.item(1).getTextContent());
                        this.error.setAdvisoryTxt(this.advisory.item(2).getTextContent());
                    
                        firstRun = false; 
                        throw new UnsupportedOperationException("Response from Chime: ");

                    }

                } catch (XPathExpressionException ex) {
                    Logger.getLogger(ServiceInterceptor.class.getName()).log(Level.SEVERE, null, ex);

                }

            }

        return false;
    }

    @Override
    public boolean handleFault(MessageContext mc) throws WebServiceClientException {
        return false;
    }

    @Override
    public void afterCompletion(MessageContext mc, Exception excptn) throws WebServiceClientException {


    }
}
