
package com.chime.models;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
public class ChimeAdvisoryError {
    
    private String advisoryCat;
    private String advisoryTxt;
    private String advisoryCde;

    /**
     * @return the advisoryCat
     */
    public String getAdvisoryCat() {
        return advisoryCat;
    }

    /**
     * @param advisoryCat the advisoryCat to set
     */
    public void setAdvisoryCat(String advisoryCat) {
        this.advisoryCat = advisoryCat;
    }

    /**
     * @return the advisoryTxt
     */
    public String getAdvisoryTxt() {
        return advisoryTxt;
    }

    /**
     * @param advisoryTxt the advisoryTxt to set
     */
    public void setAdvisoryTxt(String advisoryTxt) {
        this.advisoryTxt = advisoryTxt;
    }

    /**
     * @return the advisoryCde
     */
    public String getAdvisoryCde() {
        return advisoryCde;
    }

    /**
     * @param advisoryCde the advisoryCde to set
     */
    public void setAdvisoryCde(String advisoryCde) {
        this.advisoryCde = advisoryCde;
    }
    
    
    
    
    
}
