
package com.chime.models;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
public class ChimeFormQuery {
    
    private ChimePerson person;
    private ChimeOrganization organization;
    private String sources;
    private String entities;

    /**
     * @return the person
     */
    public ChimePerson getPerson() {
        return person;
    }

    /**
     * @param person the person to set
     */
    public void setPerson(ChimePerson person) {
        this.person = person;
    }



    /**
     * @return the sources
     */
    public String getSources() {
        return sources;
    }

    /**
     * @param sources the sources to set
     */
    public void setSources(String sources) {
        this.sources = sources;
    }

    /**
     * @return the entities
     */
    public String getEntities() {
        return entities;
    }

    /**
     * @param entities the entities to set
     */
    public void setEntities(String entities) {
        this.entities = entities;
    }

    /**
     * @return the organization
     */
    public ChimeOrganization getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(ChimeOrganization organization) {
        this.organization = organization;
    }
    
    
    
    
    
}
