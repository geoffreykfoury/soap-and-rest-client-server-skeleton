package com.chime.models;

import java.util.ArrayList;

public class NewsAPIMultiSearch {
    
    private ArrayList<String> sources;

    /**
     * @return the sources
     */
    public ArrayList<String> getSources() {
        return sources;
    }

    /**
     * @param sources the sources to set
     */
    public void setSources(ArrayList<String> sources) {
        this.sources = sources;
    }
    
}
