
package com.chime.models;

import java.util.List;

public class ChimeOrganization {

    private String sourceId;
    private String employeeId;
    private String country;
    private String dateIncorporated;
    private String status;
    private String type;
    private String stateOfCharter;
    private List alias;
    private String orgName;
    private String addressType;
    private String Address;
    private String officerName;
    private String officerTitle;
    private String dataItemId;


    /**
     * @return the sourceId
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the dateIncorporated
     */
    public String getDateIncorporated() {
        return dateIncorporated;
    }

    /**
     * @param dateIncorporated the dateIncorporated to set
     */
    public void setDateIncorporated(String dateIncorporated) {
        this.dateIncorporated = dateIncorporated;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the stateOfCharter
     */
    public String getStateOfCharter() {
        return stateOfCharter;
    }

    /**
     * @param stateOfCharter the stateOfCharter to set
     */
    public void setStateOfCharter(String stateOfCharter) {
        this.stateOfCharter = stateOfCharter;
    }

    /**
     * @return the alias
     */
    public List getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(List alias) {
        this.alias = alias;
    }


    /**
     * @return the addressType
     */
    public String getAddressType() {
        return addressType;
    }

    /**
     * @param addressType the addressType to set
     */
    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

  
    public String getAddress() {
        return Address;
    }

    /**
     * @param Address the Address to set
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     * @return the officerName
     */
    public String getOfficerName() {
        return officerName;
    }

    /**
     * @param officerName the officerName to set
     */
    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    /**
     * @return the dataItemId
     */
    public String getDataItemId() {
        return dataItemId;
    }

    /**
     * @param dataItemId the dataItemId to set
     */
    public void setDataItemId(String dataItemId) {
        this.dataItemId = dataItemId;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the officerTitle
     */
    public String getOfficerTitle() {
        return officerTitle;
    }

    /**
     * @param officerTitle the officerTitle to set
     */
    public void setOfficerTitle(String officerTitle) {
        this.officerTitle = officerTitle;
    }
}
