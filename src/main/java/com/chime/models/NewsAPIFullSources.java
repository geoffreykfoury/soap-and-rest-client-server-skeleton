
package com.chime.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
public class NewsAPIFullSources implements Serializable{
    
    private String status;
    private ArrayList<NewsAPIPartSources> sources;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the sources
     */
    public ArrayList<NewsAPIPartSources> getSources() {
        return sources;
    }

    /**
     * @param sources the sources to set
     */
    public void setSources(ArrayList<NewsAPIPartSources> sources) {
        this.sources = sources;
    }
    
}
