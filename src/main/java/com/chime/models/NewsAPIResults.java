
package com.chime.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
public class NewsAPIResults implements Serializable{
    
    private String status;
    private String source;
    private String sortBy;
    private ArrayList<NewsAPIArticles> articles;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the sortBy
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * @param sortBy the sortBy to set
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * @return the articles
     */
    public ArrayList<NewsAPIArticles> getArticles() {
        return articles;
    }

    /**
     * @param articles the articles to set
     */
    public void setArticles(ArrayList<NewsAPIArticles> articles) {
        this.articles = articles;
    }
    
    
}
