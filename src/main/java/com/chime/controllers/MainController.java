package com.chime.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Controller
public class MainController {

    @RequestMapping("/")
    public String mainHome() {
        return "home";
    }

    @RequestMapping("/query")
    public String personTest() {
        return "query-search";
    }

    @RequestMapping("/search-results")
    public String personResults() {
        return "search-results";
    }


}
