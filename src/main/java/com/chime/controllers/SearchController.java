package com.chime.controllers;

import chime.wsdl.GenericSearchResultType;
import chime.wsdl.GenericSummaryItemType;
import chime.wsdl.OrganizationSummaryItemDataType;
import chime.wsdl.OrganizationType;
import chime.wsdl.PersonSummaryItemDataType;
import chime.wsdl.PersonType;
import chime.wsdl.RetrieveRequestType;
import chime.wsdl.RetrieveResultType;
import chime.wsdl.SearchRequestType;
import chime.wsdl.SearchResultType;
import com.chime.models.ChimeAdvisoryError;
import com.chime.models.ChimeFormQuery;
import com.chime.models.ChimeOrganization;
import com.chime.models.ChimePerson;
import com.chime.services.NewsAPIClient;
import com.chime.services.OrgsClient;
import com.chime.services.ChimeServiceClient;
import com.chime.services.ChimeRqstCrte;
import com.chime.services.OrgRetrieveCreate;
import com.chime.services.PrsnRetrieveCreate;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceTransportException;

/**
 *
 * @author Geoffrey Ford Kfoury
 */
@Controller
public class SearchController {

    @Autowired
    ChimeRqstCrte chimeCrte;

    @Autowired
    PrsnRetrieveCreate prsnRetrieve;

    @Autowired
    ChimeServiceClient chimeClient;

    @Autowired
    OrgsClient orgSearchClient;

    @Autowired
    NewsAPIClient headlines;

    @Autowired
    OrgRetrieveCreate orgRetrieve;

    @RequestMapping(value = "/query/results", method = RequestMethod.POST)
    public String showPrsnRslts(@ModelAttribute("formQ") ChimeFormQuery fq, BindingResult result, Model model) {

        try {
            HashMap<String, ArrayList> searchResults = chimeSearch(fq);

            for (Object key : searchResults.keySet()) {

                switch (key.toString()) {

                    case "PERSON":

                        ChimePerson newFella = (ChimePerson) searchResults.get("PERSON").get(0);
                        model.addAttribute("person", newFella);
                        break;

                    case "ORGANIZATION":
                        ArrayList<Object> fullOrg = searchResults.get("ORGANIZATION");

                        model.addAttribute("org", fullOrg);

                }

            }
        } catch (UnsupportedOperationException ex) {
            ChimeAdvisoryError error;
            error = chimeClient.getError();
            model.addAttribute("error", error);
        }
        return "search-results";
    }

    public HashMap chimeSearch(ChimeFormQuery fq) throws UnsupportedOperationException {

        HashMap<String, ArrayList> resultMap = new HashMap();

        SearchRequestType rqst = chimeCrte.crteChimeRqst(fq);

        SearchResultType chimeRslt = chimeClient.searchChime(rqst);

        for (int i = 0; i < chimeRslt.getAbstractSearchSubResult().size(); i++) {

            JAXBElement<GenericSearchResultType> hello = (JAXBElement<GenericSearchResultType>) chimeRslt.getAbstractSearchSubResult().get(i);
            GenericSearchResultType narrowedResult = hello.getValue();

            //Should this be a switch statement? - research differences in optimization
            for (int t = 0; t < narrowedResult.getSummaryItem().size(); t++) {

                GenericSummaryItemType genericSummaryItem = narrowedResult.getSummaryItem().get(t);

                if (genericSummaryItem.getSummaryItemMetadata().getDataItemIdentifier().getEntityId().toString().equals("PERSON")) {
                    ChimePerson person = new ChimePerson();

                    PersonSummaryItemDataType personSummaryItem = (PersonSummaryItemDataType) narrowedResult.getSummaryItem().get(t).getSummaryItemData().getAbstractSummaryItemData().getValue();

                    ArrayList results = new ArrayList();
                    person.setGivenName(personSummaryItem.getPersonName());
                    person.setDataId(personSummaryItem.getRecordId());
                    person.setGender(personSummaryItem.getGender());
                    person.setHeight(personSummaryItem.getHeight());
                    person.setNationality(personSummaryItem.getNationality());
                    person.setDob(personSummaryItem.getDateOfBirth().toString());

                    Field[] fields = person.getClass().getDeclaredFields();
                    for (Field f : fields) {
                        f.setAccessible(true);
                        try {
                            if (f.get(person).equals("")) {
                                f.set(person, "Not Provided");

                            }
                        } catch (IllegalArgumentException ex) {
                            Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    results.add(person);
                    resultMap.put("PERSON", results);

                }

                if (genericSummaryItem.getSummaryItemMetadata().getDataItemIdentifier().getEntityId().toString().equals("ORGANIZATION")) {
                    ChimeOrganization printOrg = new ChimeOrganization();

                    OrganizationSummaryItemDataType orgSummaryItem = (OrganizationSummaryItemDataType) narrowedResult.getSummaryItem().get(t).getSummaryItemData().getAbstractSummaryItemData().getValue();

                    ArrayList results = new ArrayList();
                    //initial show 
                    printOrg.setSourceId(genericSummaryItem.getSummaryItemMetadata().getDataItemIdentifier().getSourceId().value());
                    printOrg.setOrgName(orgSummaryItem.getOrganizationName());
                    printOrg.setCountry(orgSummaryItem.getOrganizationCountry());

                    if (orgSummaryItem.getIncorporationDate() != null) {
                        printOrg.setDateIncorporated(orgSummaryItem.getIncorporationDate().toString());
                    } else {
                        printOrg.setDateIncorporated("None Specified");
                    }

                    printOrg.setStateOfCharter(orgSummaryItem.getStateOfCharter());
                    printOrg.setDataItemId(genericSummaryItem.getSummaryItemMetadata().getDataItemIdentifier().getDataItemId());

                    results.add(printOrg);
                    resultMap.put("ORGANIZATION", results);

                }
            }

//            }
        }
        return resultMap;
    }
//****************************************************************************************************************************************************************
//            RETRIEVE                                                  RETRIEVE                                            RETRIEVE
//**************************************************************************************************************************************************************** 
    //NOTES:
    //NEED TO REFACTOR TO ACCOMODATE FOR THE LISTS OF INTERNAL ITEMS
    //FOR EXAMPLE, MULTIPLE ORGANIZATION - OFFICER ITEMS

    //PERSON RETRIEVE - LIGHT
    @ResponseBody
    @RequestMapping("/person/{recId}")
    public ArrayList<Object> getPrsnDetails(@PathVariable String recId) {

        ArrayList<Object> allPrsnRetrieve = prsnRtrvePrnt(recId);

        return allPrsnRetrieve;
    }

    //PERSON RETRIEVE - HEAVY
    @RequestMapping(value = "/person/retrieve/{recId}", method = RequestMethod.GET)
    public String showPrsnRslts(@PathVariable String recId, Model model) {

        ArrayList<Object> allPrsnRetrieve = prsnRtrvePrnt(recId);
        model.addAttribute("prsnRtrve", allPrsnRetrieve);

        return "person-retrieve";

    }

    //ORGRANIZATION RETRIEVE - LIGHT
    @ResponseBody
    @RequestMapping("/org/{recId}")
    public ArrayList<Object> getOrgDetails(@PathVariable String recId) {

        ArrayList<Object> allOrgRetrieve = orgRtrvePrnt(recId);

        return allOrgRetrieve;
    }

    //ORGRANIZATION RETRIEVE - HEAVY
    @RequestMapping(value = "/org/retrieve/{recId}", method = RequestMethod.GET)
    public String showOrgRslts(@PathVariable String recId, Model model) {

        ArrayList<Object> allOrgRetrieve = orgRtrvePrnt(recId);
        model.addAttribute("orgRtrve", allOrgRetrieve);

        return "organization-retrieve";

    }

    public ArrayList<Object> prsnRtrvePrnt(String recId) {

        ArrayList<Object> allPrsnRetrieve = new ArrayList();
        try {
            RetrieveRequestType retrieve = prsnRetrieve.createPrsnRetrieve(recId);
            RetrieveResultType searchResult = chimeClient.retrieveDetails(retrieve);

            for (int i = 0; i < searchResult.getDataItem().size(); i++) {

                JAXBElement<PersonType> narrowedPerson = (JAXBElement<PersonType>) searchResult.getDataItem().get(i).getAbstractDataItemData();
                PersonType personDataItemValue = narrowedPerson.getValue();
                ChimePerson prsnDtls = new ChimePerson();

                if (!personDataItemValue.getAddress().isEmpty()) {

                    String street = personDataItemValue.getAddress().get(0).getStreet();
                    String city = personDataItemValue.getAddress().get(0).getCity();
                    String state = personDataItemValue.getAddress().get(0).getState();
                    String country = personDataItemValue.getAddress().get(0).getCountry();
                    String zip = personDataItemValue.getAddress().get(0).getPostalCode();
                    prsnDtls.setAddressType(personDataItemValue.getAddress().get(0).getAddressTypeCode());

                    prsnDtls.setAddress(street + ", \n" + city + ", " + state + ", \n" + country + " " + zip);
                } else {
                    prsnDtls.setAddress("None Specified");
                    prsnDtls.setAddressType("None Specified");
                }

                prsnDtls.setDataId(searchResult.getRetrieveResultMetadata().getDataItemIdentifier().getDataItemId());
                prsnDtls.setEyeColor(personDataItemValue.getEyeColor().value());
                prsnDtls.setHairColor(personDataItemValue.getHairColor().value());

                if (personDataItemValue.getWeight() != null) {
                    prsnDtls.setWeight(personDataItemValue.getWeight().toString());
                } else {
                    prsnDtls.setWeight("None Provided");
                }

                if (personDataItemValue.getHeight() != null) {
                    prsnDtls.setHeight(personDataItemValue.getHeight());
                } else {
                    prsnDtls.setHeight("None Provided");
                }

                if (!personDataItemValue.getPersonName().isEmpty()) {
                    if (personDataItemValue.getPersonName().get(0).getMiddleName() != null) {
                        prsnDtls.setFullName(personDataItemValue.getPersonName().get(0).getGivenName() + " "
                                + personDataItemValue.getPersonName().get(0).getMiddleName() + " " + personDataItemValue.getPersonName().get(0).getSurname());
                    } else {
                        prsnDtls.setFullName(personDataItemValue.getPersonName().get(0).getGivenName() + " "
                                + personDataItemValue.getPersonName().get(0).getSurname());
                    }
                } else {
                    prsnDtls.setFullName("None Specified");
                }

                allPrsnRetrieve.add(prsnDtls);

            }
        } catch (WebServiceTransportException ex) {
            Logger.getLogger(OrganizationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedOperationException ex) {
            ChimeAdvisoryError error = chimeClient.getError();
            allPrsnRetrieve.add(error);
        }

        return allPrsnRetrieve;

    }

    public ArrayList<Object> orgRtrvePrnt(String recId) {

        ArrayList<Object> allOrgRetrieve = new ArrayList();
        try {
            RetrieveRequestType retrieve = orgRetrieve.createOrgRetrieve(recId);
            RetrieveResultType searchResult = chimeClient.retrieveDetails(retrieve);

            for (int i = 0; i < searchResult.getDataItem().size(); i++) {

                JAXBElement<OrganizationType> narrowedOrg = (JAXBElement<OrganizationType>) searchResult.getDataItem().get(i).getAbstractDataItemData();
                OrganizationType orgDataItemValue = narrowedOrg.getValue();
                ChimeOrganization orgDtls = new ChimeOrganization();

                if (!orgDataItemValue.getAddress().isEmpty()) {

                    String street = orgDataItemValue.getAddress().get(0).getStreet();
                    String city = orgDataItemValue.getAddress().get(0).getCity();
                    String state = orgDataItemValue.getAddress().get(0).getState();
                    String country = orgDataItemValue.getAddress().get(0).getCountry();
                    String zip = orgDataItemValue.getAddress().get(0).getPostalCode();
                    orgDtls.setAddressType(orgDataItemValue.getAddress().get(0).getAddressTypeCode());

                    orgDtls.setAddress(street + ", \n" + city + ", " + state + ", \n" + country + " " + zip);
                } else {
                    orgDtls.setAddress("None Specified");
                    orgDtls.setAddressType("None Specified");
                }

                orgDtls.setDataItemId(searchResult.getRetrieveResultMetadata().getDataItemIdentifier().getDataItemId());
                orgDtls.setSourceId(searchResult.getRetrieveResultMetadata().getDataItemIdentifier().getSourceId().value());

                orgDtls.setStatus(orgDataItemValue.getOrganizationStatus());
                orgDtls.setStateOfCharter(orgDataItemValue.getStateOfCharter());
                orgDtls.setType(orgDataItemValue.getOrganizationType());
                orgDtls.setCountry(orgDataItemValue.getIncorporationCountry());
                orgDtls.setOrgName(orgDataItemValue.getOrganizationName().get(0).getName());

                if (!orgDataItemValue.getOrganizationOfficer().isEmpty()) {
                    orgDtls.setOfficerTitle(orgDataItemValue.getOrganizationOfficer().get(0).getTitle());

                    if (orgDataItemValue.getOrganizationOfficer().get(0).getMiddleName() != null) {
                        orgDtls.setOfficerName(orgDataItemValue.getOrganizationOfficer().get(0).getGivenName() + " "
                                + orgDataItemValue.getOrganizationOfficer().get(0).getMiddleName() + " " + orgDataItemValue.getOrganizationOfficer().get(0).getSurname());
                    } else {
                        orgDtls.setOfficerName(orgDataItemValue.getOrganizationOfficer().get(0).getGivenName() + " "
                                + orgDataItemValue.getOrganizationOfficer().get(0).getSurname());
                    }
                } else {
                    orgDtls.setOfficerName("None Specified");
                }
                allOrgRetrieve.add(orgDtls);
            }
        } catch (WebServiceTransportException ex) {
            Logger.getLogger(OrganizationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedOperationException ex) {
            ChimeAdvisoryError error = chimeClient.getError();
            allOrgRetrieve.add(error);
        }

        return allOrgRetrieve;

    }
}
//**************************************************************************************************************************************************************** 
//           END RETRIEVE                                                END RETRIEVE                                          END RETRIEVE   
//****************************************************************************************************************************************************************
