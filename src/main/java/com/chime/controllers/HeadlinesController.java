//This is an example class to implement with an external REST API

package com.chime.controllers;

import com.chime.models.NewsAPIArticles;
import com.chime.models.NewsAPIFullSources;
import com.chime.models.NewsAPIMultiSearch;
import com.chime.models.NewsAPIPartSources;
import com.chime.models.NewsAPIResults;
import java.io.IOException;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;


@Controller
public class HeadlinesController {
    
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/headlines")
    public String mainHome() {
        return "headlines";
    }

    @ResponseBody
    @RequestMapping("/headlines/{sort}/{source}")
    public ArrayList<NewsAPIArticles> getHeadlines(@PathVariable String sort, @PathVariable String source) throws IOException {

        NewsAPIResults articles = restTemplate.getForObject("https://newsapi.org/v1/articles?source=" + source + "&sortBy=" + sort + "&apiKey=f70ba31aca1a4bab88d54a766c9ba433", NewsAPIResults.class);

        return articles.getArticles();
    }

    @ResponseBody
    @RequestMapping("/headlines/sources")
    public ArrayList<NewsAPIPartSources> getAllSources() throws IOException {

        NewsAPIFullSources sources = restTemplate.getForObject("https://newsapi.org/v1/sources", NewsAPIFullSources.class);

        return sources.getSources();

    }

    @ResponseBody
    @RequestMapping("/headlines/filter/{category}/{language}/{country}")
    public ArrayList<NewsAPIPartSources> getSources(@PathVariable String category, @PathVariable String language, @PathVariable String country) throws IOException {

        NewsAPIFullSources sources = restTemplate.getForObject("https://newsapi.org/v1/sources?category=" + category + "&language=" + language + "&country=" + country, NewsAPIFullSources.class);

        return sources.getSources();

    }

    @ResponseBody
    @RequestMapping("/headlines/{sort}")
    public ArrayList<NewsAPIArticles> getHeadlinesMultiSource(@RequestBody NewsAPIMultiSearch sources, @PathVariable String sort) {

        NewsAPIResults articles = restTemplate.getForObject("https://newsapi.org/v1/articles?source=" + sources.getSources().get(0) + "&sortBy=" + sort + "&apiKey=f70ba31aca1a4bab88d54a766c9ba433", NewsAPIResults.class);

        for (int i = 1; i < sources.getSources().size() ; i++) {
            articles.getArticles().addAll(restTemplate.getForObject("https://newsapi.org/v1/articles?source=" + sources.getSources().get(i) + "&sortBy=" + sort + "&apiKey=f70ba31aca1a4bab88d54a766c9ba433", NewsAPIResults.class).getArticles());
        }
        return articles.getArticles();
    }

}
