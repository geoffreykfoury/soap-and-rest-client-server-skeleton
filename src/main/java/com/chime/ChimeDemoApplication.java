package com.chime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//@ComponentScan("com.confroom.DAO.MainDAO")
public class ChimeDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChimeDemoApplication.class, args);
    }

}
