# SOAP and REST - All Purpose Framework

This is the redacted version of a client facing service that sits in front of a large interfaces application.  This is the skeleton and prototype of the final version, including graphics, layout, and functionality.

  - SOAP and REST
  - API Producer and Consumer
  - Responsive and Mobile
  - Aynchronous and No Page Refresh 

### SOAP 

  - JAXB2 and Spring WebServiceGatewaySupport for most operations
  - Ready and functional with Abstract and SubstitutionGroups schema elements
  - ClientInterceptor for backend error response
  - Dynamic WSDL configuration in POM

### REST 
  - API consumer and producer 

### BASICS

 - Spring Security, Spring Boot, Spring WS Core, JAXB
 - JQuery, AJAX, Bootstrap
 
![Query](https://i.imgur.com/m9JkAcn.jpg)
![API](http://i.imgur.com/9YvjyNS.jpg?1)
![Login](http://i.imgur.com/29Pbc4w.jpg?1)
